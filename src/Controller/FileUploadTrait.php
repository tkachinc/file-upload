<?php
namespace TkachInc\FileUpload\Controller;

use TkachInc\FileUpload\Classes\FileUploaded;
use TkachInc\FileUpload\Classes\UploadFile;
use TkachInc\FileUpload\Classes\Validations\Validation;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
trait FileUploadTrait
{
	/**
	 * @return mixed
	 */
	abstract protected function getUrl();

	/**
	 * @return mixed
	 */
	abstract protected function getUploadPath();

	/**
	 * @return mixed
	 */
	abstract protected function getNameKey();

	/**
	 * @param FileUploaded $file
	 * @return mixed
	 */
	abstract protected function worker(FileUploaded $file);

	/**
	 * @param Validation $validation
	 * @return array
	 */
	abstract protected function addValidates(Validation $validation);

	public function run()
	{
		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		ini_set("auto_detect_line_endings", true);

		$uploader = new UploadFile($this->getNameKey());
		$validation = new Validation();
		$this->addValidates($validation);
		$result = $uploader->upload($validation, $this->getUploadPath(), true);

		$response = [];
		/** @var FileUploaded $file */
		foreach ($result as $file) {
			$result = $this->worker($file);

			$smallPreview = $file->getSmallPreview();
			$preview = $file->getPreview();
			$response[] = [
				'url'          => $preview,
				'thumbnailUrl' => $smallPreview,
				'name'         => $file->getName(),
				'type'         => $file->getMime(),
				'size'         => $file->getSize(),
			];
		}

		$this->send(['files' => $response, 'result' => $result]);
	}

	/**
	 * @param $response
	 */
	protected function send($response)
	{
		http_response_code(200);
		header('Content-type: application/json');
		echo json_encode($response);
		fastcgi_finish_request();
	}
}