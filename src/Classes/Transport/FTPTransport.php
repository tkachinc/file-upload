<?php
namespace TkachInc\FileUpload\Classes\Transport;

/**
 * Class FTPTransport
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FTPTransport
{
	protected $cdnServer, $cdnUser, $cdnPassword, $cdnDir, $cdnUpdateUrl;

	public function getCdnUpdateUrl()
	{
		return $this->cdnUpdateUrl;
	}

	/**
	 * @param $ftp
	 * @param $directory_check
	 * @return bool
	 */
	public static function isDir($ftp, $directory_check)
	{
		$directory_original = ftp_pwd($ftp);
		if (@ftp_chdir($ftp, $directory_check)) {
			ftp_chdir($ftp, $directory_original);

			return true;
		} else {
			return false;
		}
	}

	public function getConnection()
	{
		return $this->ftp;
	}

	/**
	 * @param            $url
	 * @param bool|false $background
	 * @return bool
	 */
	public static function httpGet($url, $background = false)
	{
		if ($background == true) {
			exec('wget -q -o /dev/null ' . $url . ' &');

			return true;
		} else {
			exec('wget ' . $url, $output);

			return $output;
		}
	}

	protected $ftp;

	/**
	 * @param $cdnServer
	 * @param $cdnUpdateUrl
	 * @param $cdnUser
	 * @param $cdnPassword
	 * @param $cdnDir
	 * @throws \Exception
	 */
	public function __construct($cdnServer, $cdnUpdateUrl, $cdnUser, $cdnPassword, $cdnDir)
	{
		$this->cdnServer = $cdnServer;
		$this->cdnUser = $cdnUser;
		$this->cdnPassword = $cdnPassword;
		$this->cdnDir = $cdnDir;
		$this->cdnUpdateUrl = $cdnUpdateUrl;

		$this->ftp = ftp_connect($this->cdnServer);
		if (!$this->ftp) {
			throw new \Exception('Error connect');
		}

	}

	public function login()
	{
		if (!ftp_login($this->ftp, $this->cdnUser, $this->cdnPassword)) {
			throw new \Exception('Error login');
		}
	}

	/**
	 * @param $fileServerDir
	 * @param $fileLocalPath
	 * @param $fileServerName
	 * @throws \Exception
	 */
	public function upload($fileServerDir, $fileLocalPath, $fileServerName)
	{
		if (static::isDir(
				$this->ftp,
				$this->cdnDir . '/' . $fileServerDir
			) || ftp_mkdir($this->ftp, $this->cdnDir . '/' . $fileServerDir)
		) {
			if (ftp_chdir($this->ftp, $this->cdnDir . '/' . $fileServerDir)) {
				//$file = fopen($fileLocalPath, 'rb');
				$result = ftp_put($this->ftp, $fileServerName, $fileLocalPath, FTP_BINARY);
				//fclose($file);

				if (!$result) {
					throw new \Exception('Error upload');
				}
			} else {
				throw new \Exception('Error server dir');
			}
		} else {
			throw new \Exception('Error create dir');
		}
	}

	/**
	 * @param string $localName
	 * @param string $remoteName
	 * @return bool
	 */
	public function download($localName, $remoteName)
	{
		return ftp_get($this->ftp, $localName, $remoteName, FTP_BINARY);
	}

	/**
	 * @param $fileServerDir
	 * @param $fileServerName
	 * @return bool
	 */
	public function delete($fileServerDir, $fileServerName)
	{
		ftp_chdir($this->ftp, $this->cdnDir . '/' . $fileServerDir);

		return ftp_delete($this->ftp, $fileServerName);
	}

	/**
	 * @param string $nameFile
	 * @return bool
	 */
	public function fileExists($nameFile)
	{
		$result = true;
		$size = ftp_size($this->ftp, $nameFile);
		if ($size <= 0) {
			$result = false;
		}

		return $result;
	}

	/**
	 * @param bool $mode
	 * @return bool
	 */

	public function setPasvMode($mode)
	{
		return ftp_pasv($this->ftp, $mode);
	}

	public function __destruct()
	{
		ftp_close($this->ftp);
	}
}