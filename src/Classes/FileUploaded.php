<?php
namespace TkachInc\FileUpload\Classes;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FileUploaded
{
	protected $mime;
	protected $extension;
	protected $tmpName, $name, $size;
	protected $sourceName;
	protected $url, $smallUrl;

	/**
	 * @param $tmpName
	 * @param $name
	 * @param $size
	 * @throws FileException
	 */
	public function __construct($tmpName, $name, $size)
	{
		if (!is_uploaded_file($tmpName)) {
			throw new FileException('This file not uploaded: ' . $tmpName);
		}

		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$this->mime = finfo_file($finfo, $tmpName);
		finfo_close($finfo);

		$tmp = explode('.', $name);
		if (count($tmp) > 1) {
			$this->extension = end($tmp);
		} else {
			$this->extension = MimeTypeToExtension::convert($this->mime);
		}

		$this->tmpName = $tmpName;
		$this->name = $name;
		$this->sourceName = $name;
		$this->size = $size;
	}

	/**
	 * @param $url
	 */
	public function setPreview($url)
	{
		$this->url = $url;
	}

	public function getPreview()
	{
		return $this->url;
	}

	/**
	 * @param $smallUrl
	 */
	public function setSmallPreview($smallUrl)
	{
		$this->smallUrl = $smallUrl;
	}

	public function getSmallPreview()
	{
		return $this->smallUrl;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getSourceName()
	{
		return $this->sourceName;
	}

	public function getTmpName()
	{
		return $this->tmpName;
	}

	public function getSize()
	{
		return $this->size;
	}

	/**
	 * @return null
	 */
	public function getExtension()
	{
		return $this->extension;
	}

	/**
	 * @return mixed
	 */
	public function getMime()
	{
		return $this->mime;
	}

	/**
	 * @param      $uploadDir
	 * @param bool $useUploadName
	 */
	public function upload($uploadDir, $useUploadName = true)
	{
		if (!file_exists($uploadDir)) {
			mkdir($uploadDir, 0777, true);
		}
		if ($useUploadName === false) {
			$this->name = static::generateName($this->getExtension());
		} else {
			$this->name = strip_tags(str_replace(' ', '_', $this->name));
		}

		move_uploaded_file($this->getTmpName(), rtrim($uploadDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $this->name);
	}

	/**
	 * @param        $extension
	 * @param string $prefix
	 * @return string
	 */
	public static function generateName($extension, $prefix = '')
	{
		return $prefix . mt_rand(0, 1000) . time() . mt_rand(0, 1000) . '.' . $extension;
	}
}