<?php
namespace TkachInc\FileUpload\Classes\Validations;

use TkachInc\FileUpload\Classes\FileUploaded;

/**
 * Class Mime
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Mime implements IValidation
{
	protected $availableMimeTypes;

	/**
	 * @param array $availableMimeTypes
	 */
	public function __construct($availableMimeTypes = [])
	{
		$this->availableMimeTypes = $availableMimeTypes;
	}

	/**
	 * @param FileUploaded $file
	 * @return mixed|void
	 * @throws ValidationException
	 */
	public function run(FileUploaded $file)
	{
		$mime = $file->getMime();
		if (!isset($this->availableMimeTypes[$mime])) {
			throw new ValidationException('Error mime type: ' . $mime);
		}
	}
}