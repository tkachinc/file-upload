<?php
namespace TkachInc\FileUpload\Classes\Validations;

use TkachInc\FileUpload\Classes\FileUploaded;

/**
 * Class Mime
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Size implements IValidation
{
	protected $maxBytes, $minBytes;

	/**
	 * @param     $maxBytes
	 * @param int $minBytes
	 */
	public function __construct($maxBytes, $minBytes = 0)
	{
		$this->maxBytes = $maxBytes;
		$this->minBytes = $minBytes;
	}

	/**
	 * @param FileUploaded $file
	 * @return mixed|void
	 * @throws ValidationException
	 */
	public function run(FileUploaded $file)
	{
		$fileSize = $file->getSize();
		if ($fileSize < $this->minBytes || $fileSize > $this->maxBytes) {
			throw new ValidationException('Error file size: ' . $fileSize);
		}
	}
}