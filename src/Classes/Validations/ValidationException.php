<?php
namespace TkachInc\FileUpload\Classes\Validations;

/**
 * Class Validation
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ValidationException extends \Exception
{

}