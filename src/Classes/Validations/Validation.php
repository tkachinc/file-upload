<?php
namespace TkachInc\FileUpload\Classes\Validations;

use TkachInc\FileUpload\Classes\FileUploaded;
use SplObjectStorage;

/**
 * Class Validation
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Validation
{
	protected $validates;

	/**
	 *
	 */
	public function __construct()
	{
		$this->validates = new SplObjectStorage();
	}

	/**
	 * @param IValidation $validate
	 */
	public function addValidate(IValidation $validate)
	{
		$this->validates->attach($validate);
	}

	/**
	 * @param FileUploaded $file
	 * @throws ValidationException
	 */
	public function run(FileUploaded $file)
	{
		$filename = $file->getTmpName();
		if (!file_exists($filename)) {
			throw new ValidationException('Not found file');
		}

		if (!is_file($filename)) {
			throw new ValidationException('Validate only real file');
		}

		/** @var IValidation $validate */
		foreach ($this->validates as $validate) {
			$validate->run($file);
		}
	}
}