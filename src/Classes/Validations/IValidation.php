<?php
namespace TkachInc\FileUpload\Classes\Validations;

use TkachInc\FileUpload\Classes\FileUploaded;

/**
 * Class Mime
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
interface IValidation
{
	/**
	 * @param FileUploaded $file
	 * @return mixed
	 */
	public function run(FileUploaded $file);
}