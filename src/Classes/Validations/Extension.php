<?php
namespace TkachInc\FileUpload\Classes\Validations;

use TkachInc\FileUpload\Classes\FileUploaded;

/**
 * Class Mime
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Extension implements IValidation
{
	protected $availableExtensions;

	/**
	 * @param array $availableExtensions
	 */
	public function __construct($availableExtensions = [])
	{
		$this->availableExtensions = $availableExtensions;
	}

	/**
	 * @param FileUploaded $file
	 * @return mixed|void
	 * @throws ValidationException
	 */
	public function run(FileUploaded $file)
	{
		$extension = strtolower($file->getExtension());
		if (!isset($this->availableExtensions[$extension])) {
			throw new ValidationException('Error extension type: ' . $extension);
		}
	}
}