<?php
namespace TkachInc\FileUpload\Classes;

use TkachInc\FileUpload\Classes\Validations\Validation;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class UploadFile
{
	protected $files;

	/**
	 * @param                 $nameKey
	 * @throws FileException
	 * @throws Validations\ValidationException
	 */
	public function __construct($nameKey)
	{
		if (!isset($_FILES[$nameKey]['tmp_name']) ||
			!isset($_FILES[$nameKey]['error']) ||
			!isset($_FILES[$nameKey]['name']) ||
			!isset($_FILES[$nameKey]['size'])
		) {
			throw new FileException('Not found $_FILES: ' . $nameKey);
		}

		$this->files = new \SplObjectStorage();
		if (is_array($_FILES[$nameKey]['error'])) {
			foreach ($_FILES[$nameKey]['error'] as $key => $error) {
				if ($error == UPLOAD_ERR_OK &&
					isset($_FILES[$nameKey]['tmp_name'][$key], $_FILES[$nameKey]['name'][$key], $_FILES[$nameKey]['size'][$key])
				) {
					$tmpName = $_FILES[$nameKey]['tmp_name'][$key];
					$name = $_FILES[$nameKey]['name'][$key];
					$size = $_FILES[$nameKey]['size'][$key];

					$this->files->attach(new FileUploaded($tmpName, $name, $size));
				}
			}
		} else {
			$tmpName = $_FILES[$nameKey]['tmp_name'];
			$name = $_FILES[$nameKey]['name'];
			$size = $_FILES[$nameKey]['size'];

			$this->files->attach(new FileUploaded($tmpName, $name, $size));
		}
	}

	/**
	 * @param Validation $validation
	 * @param            $uploadDir
	 * @param bool|true $useUploadName
	 * @return \Generator
	 * @throws Validations\ValidationException
	 */
	public function upload(Validation $validation, $uploadDir, $useUploadName = true)
	{
		/** @var FileUploaded $file */
		foreach ($this->files as $file) {
			$validation->run($file);
			$file->upload($uploadDir, $useUploadName);
			yield $file;
		}
	}
}